obj-m += ultra_snd.o

EXTRA_CFLAGS+=-I$(KSRC)/arch/arm/mach-parrot6/

all:
	make -C $(KSRC) M=$(PWD) modules

clean:
	$(RM) $(CLEANMOD) *.o *.ko *.mod.c Module*.symvers Module.markers modules.order
	$(RM) -R .tmp*
